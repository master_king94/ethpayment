const express = require('express');
const router = express.Router();
const walletGeneratorController = require('../controllers/walletGenerateController');

router.get('/',walletGeneratorController.generateWallet);


module.exports=router;