const express = require('express');
const router = express.Router();
const walletTransactionController = require('../controllers/walletTransactionController');


router.post('/',walletTransactionController.withdraw);
router.post('/token',walletTransactionController.withdrawToken);

module.exports=router;