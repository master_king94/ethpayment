const express = require('express');
const router = express.Router();
const walletBalanceController = require('../controllers/walletBalanceController');


router.post('/',walletBalanceController.balance);
router.post('/token',walletBalanceController.tokenBalance);

module.exports=router;