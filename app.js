const express = require('express');
const app = express();

const walletGeneratorRouter = require('./router/walletGenerateRouter');
const walletBalanceRouter = require('./router/walletBalanceRouter');
const walletTransactionRouter = require('./router/walletTransactionRouter');

app.use(express.json());

app.use('/wallet/generate',walletGeneratorRouter);
app.use('/wallet/balance',walletBalanceRouter);
app.use('/wallet/withdraw',walletTransactionRouter);

module.exports=app;