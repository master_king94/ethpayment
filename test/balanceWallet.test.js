const request = require('supertest')
const app = require('../app')

describe('Balance Wallet Endpoints', () => {
  it('should get a wallet balance', async () => {
    const res = await request(app)
      .post('/wallet/balance').send({
        "walletAddress":"0x3028cf7657EfcAB8975c3c21451dcDC76b411Fd0"
      })
    expect(res.statusCode).toEqual(200)
    expect(parseFloat(res.body.data.balance)).toBeGreaterThan(0)
  })
})

describe('Balance Token Wallet Endpoints', () => {
    it('should get a wallet balance of token', async () => {
      const res = await request(app)
        .post('/wallet/balance').send({
          "walletAddress":"0x3028cf7657EfcAB8975c3c21451dcDC76b411Fd0",
          "contract":"0x01BE23585060835E02B77ef475b0Cc51aA1e0709"
        })
      expect(res.statusCode).toEqual(200)
      expect(parseFloat(res.body.data.balance)).toBeGreaterThan(0)
    })
  })