const request = require('supertest')
const app = require('../app')

describe('withdraw eth Wallet Endpoints', () => {
    it('should withdraw eth from wallet', async () => {
      const res = await request(app)
        .post('/wallet/withdraw').send({
          "fromAddress":"0xF1537C1489Cf6A9d9ef35113E643bdDAbD05d43C",
          "toAddress":"0x3D52bB41f46146E18cb8055bc4F91fF80A0F6926",
          "amount":0.005,
          "privateKey":"0xfddd58fab932bf88dc928d66c0eb80db7d3a42b5cd753a291dccbbda9c34e94d"
        })
      expect(res.statusCode).toEqual(200)
      expect(res.body.data).toHaveProperty('txId')
    },60000)
})

describe('withdraw token Wallet Endpoints', () => {
    it('should withdraw token from wallet', async () => {
      const res = await request(app)
        .post('/wallet/withdraw/token').send({
          "fromAddress":"0xF1537C1489Cf6A9d9ef35113E643bdDAbD05d43C",
          "toAddress":"0x3D52bB41f46146E18cb8055bc4F91fF80A0F6926",
          "amount":0.0001,
          "privateKey":"0xfddd58fab932bf88dc928d66c0eb80db7d3a42b5cd753a291dccbbda9c34e94d",
          "contract":"0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984"
        })
      expect(res.statusCode).toEqual(200)
      expect(res.body.data).toHaveProperty('txId')
    },60000)
})