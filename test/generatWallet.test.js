const request = require('supertest')
const app = require('../app')

describe('Generate Wallet Endpoints', () => {
  it('should generate a new wallet', async () => {
    const res = await request(app)
      .get('/wallet/generate').send()
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('data')
  })
})