
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.RPC));
const abiJson = require('../config/abi.json');

/**
 * 
 * @param {Request} req 
 * @param {Response} res
 *  
 * @returns {Success|Error}
 * 
 * This function returns the Ethereum balance of the wallet and does not check the token balance
 */
const balance = async(req,res)=>{
    try{
        const walletAddress = req.body.walletAddress;
        let balance = await web3.eth.getBalance(walletAddress);
        balance = web3.utils.fromWei(balance);
        res.send({'data':{'balance':balance}})
    }catch(e){
        res.status().send({'error':e.message})
    }
}

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
 * @returns {Success|Error}
 * 
 * This function checks the token balance of the wallet base of the smart contract address you send to this endpoint
 */
const tokenBalance = async(req,res)=>{
    try{
        let contractAddress = req.body.contract;
        let walletAddress = req.body.walletAddress;
        const contract = new web3.eth.Contract(abiJson,contractAddress);
        const balance = await contract.methods.balanceOf(walletAddress).call();
        const decimal = await contract.methods.decimals().call();
        const realBalance = balance / 10**decimal;
        res.send({'data':{'balance':realBalance}})
    }catch(e){
        res.status(400).send({'error':e.message})
    }
}



module.exports={
    balance,
    tokenBalance,
}