
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.RPC));

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
 * @returns {Success|Error}
 * 
 * This function generates a wallet base of a random string
 */
const generateWallet = async(req,res)=>{
    try{
        const random = await makeid(32);
        const account = web3.eth.accounts.create(random);
        res.send({'data':{'address':account.address,'private_key':account.privateKey}})
    }catch(e){
        res.status(400).send({'error':e.message})
    }
}
/**
 * 
 * @param {Int} length 
 * 
 * @returns {String} result
 * 
 * This function generates a random string
 */
async function makeid(length) {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

module.exports={
    generateWallet,
}