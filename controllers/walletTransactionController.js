
const Web3 = require('web3');
const ethers = require('ethers');

const web3 = new Web3(new Web3.providers.HttpProvider(process.env.RPC));
const abiJson = require('../config/abi.json');

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
 * This function withdraws eth from the wallet with the private key of that wallet and returns the transaction Id that can check on etherscan.
 * 
 * Important: Can be more Efficient if this funcation run in background proccess
 * 
 * @returns {Success|Error}
 */
const withdraw = async(req,res)=>{
    try {
        const from = req.body.fromAddress;
        const to = req.body.toAddress;
        const amount = req.body.amount;
        const privateKey = req.body.privateKey;
        const tx = {
            from: from,
            to: to,
            gas: 21000,
            value: web3.utils.toWei(amount.toString(), "ether"),
        };
        const signPromise = web3.eth.accounts.signTransaction(tx, privateKey);
        signPromise.then((signedTx) => {
            const sentTx = web3.eth.sendSignedTransaction(signedTx.raw || signedTx.rawTransaction);
            sentTx.on("receipt", receipt => {
                res.send({'data': {'txId': receipt.transactionHash}})
            });
            sentTx.on("error", err => {
                res.send({'error': err.message})
            });
        }).catch((err) => {
            res.send({'error': err.message})
        });
    }catch (err) {
        res.send({'error': err.message})
    }
}
/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 * 
 * This function withdraw token from one wallet to another wallet base on the private key of the source wallet
 * 
 * Important: Can be more Efficient if this funcation run in background proccess
 * 
 * @returns {Success|Error}
 */
const withdrawToken = async(req,res)=>{
    try {
        const from = req.body.fromAddress;
        const to = req.body.toAddress;
        const amount = req.body.amount;
        const privateKey = req.body.privateKey;
        const contractAddress = req.body.contract;

        const contract =  new web3.eth.Contract(abiJson, contractAddress);
        let decimal = await contract.methods.decimals().call();
        let tokenAmount = ethers.utils.parseUnits(amount.toString(), decimal);
        tokenAmount = tokenAmount._hex;
        const gasLimit = await contract.methods.transfer(to,tokenAmount).estimateGas({from:from});
        const data = await contract.methods.transfer(to,tokenAmount).encodeABI();
        const tx = {
            from: from,
            to: contractAddress,
            gas: gasLimit,
            data: data,
        };
        web3.eth.accounts.signTransaction(tx, privateKey).then((signedTx) => {
            const sentTx = web3.eth.sendSignedTransaction(signedTx.raw || signedTx.rawTransaction);
            sentTx.on("receipt", receipt => {
                res.send({'data': {'txId': receipt.transactionHash}})
            });
            sentTx.on("error", err => {
                res.send({'error': err.message})
            });
        }).catch((err) => {
            res.send({'error': err.message})
        });

    } catch (error) {
        res.status(400).send({'error':error.message});
    }
}

module.exports={
    withdraw,
    withdrawToken
}